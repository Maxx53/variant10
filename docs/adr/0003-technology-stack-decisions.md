# 3. Technology Stack Decisions

2021-12-05

## Status

Accepted

## Context

Compliance of the work with all requirements of diploma task.

## Decision

| Criterria 				| Selected tool 		| Reason 	   |
|---------------------------|-----------------------|--------------|
| SCM     	 				| GitLab        		| Known and easy to use. |       
| Tests     				| xUnit tests   		| Most popular test framework for dotNET applications. Tests will be run inside CI/CD pipeline. |       
| Quality gate 				| SonarQube    			| Best choice as it often used in production. Will be spinned inside EKS cluster.|
| IaC  						| Terraform, Helm		| Infrustructure will be provisioned outside CI/CD pipeline. Terraform will run and store state on the local machine. *.tf and chart files will be stored in GitLab repository (iac and helm folders).|  
| Orchestration				| EKS  					| Managed services are easier to use.|
| Monitoring				| CloudWatch  			| Managed services are easier to use. Be sure to implement the 4 golden signals concept.|
| Logging					| CloudWatch  			| Managed services are easier to use.|
| Runtime/Deployment		| GitLab CI/CD			| Known and easy to use.|
| Scalability/redundancy	| EKS, KEDA  			| Managed services are easier to use. KEDA is the only autoscaler that works with CloudWatch. |
| Cloud and Cost efficiency	| AWS built-in tools	| Determined by cloud platform choice.|


## Consequences

The right choice of tools will allow to comply all the requirements of the diploma task in an optimal way and reduce the time for its implementation.