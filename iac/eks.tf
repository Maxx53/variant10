locals {
  cluster_name = "var10-eks"
}

module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  version         = "17.24.0"
  cluster_name    = local.cluster_name
  cluster_version = "1.21"
  subnets         = module.vpc.private_subnets

  vpc_id = module.vpc.vpc_id

  cluster_enabled_log_types     = ["audit", "api"]
  cluster_log_retention_in_days = 14

  workers_group_defaults = {
    root_volume_type = "gp2"
  }

  worker_groups = [
    # {
    #   name                          = "worker-group-small"
    #   instance_type                 = "t2.small"
    #   additional_security_group_ids = [aws_security_group.worker_group.id]
    #   # kubelet_extra_args            = "--node-labels=role=prod"
    #   asg_desired_capacity = 2
    #   asg_max_size         = 3
    #   asg_min_size         = 2

    # },
    {
      name                          = "worker-group-medium"
      instance_type                 = "t3a.medium"
      additional_security_group_ids = [aws_security_group.worker_group.id]
      #kubelet_extra_args            = "--node-labels=role=tools"
      asg_desired_capacity = 4
      asg_max_size         = 5
      asg_min_size         = 3
    },
  ]
}

resource "aws_iam_role_policy_attachment" "cloudwatch-attachment" {
  role = module.eks.worker_iam_role_name
  #policy_arn = "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchFullAccess"
}


data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}
