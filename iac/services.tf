#====================EXTERNALNAME SERVICES ========================#

resource "kubernetes_service" "rds_service" {

  for_each = var.app_envs

  metadata {
    name      = "rds-service"
    namespace = kubernetes_namespace.env_ns[each.key].metadata.0.name
  }
  spec {
    type          = "ExternalName"
    external_name = aws_db_instance.var10_mysql[each.key].address
  }
}
