## Prerequisites
Terraform, kubectl, git and curl need to be installed.
```bash
export AWS_ACCESS_KEY_ID=your-key-id
export AWS_SECRET_ACCESS_KEY=your-access-key
export GL_API_TOKEN=your-token
```

## Deploy infrastructure
```bash
terraform init
terraform apply --auto-approve
```

## Using remote backend on other machine
```bash
git clone https://gitlab.com/Maxx53/variant10.git
cd variant10/iac
terraform init
```

## Pushing k8s credentials to gitlab variables
```bash
cd variant10/iac
bash creds2gitlab.sh
```

## Setting kubectl config using terraform outputs
Only "admin" and "gitlab" are allowed for config name.
```bash
cd variant10/iac
bash set-config.sh admin
```

## Checking k8s resource allocation using kubectl
```bash
kubectl describe nodes | grep 'Name:\|Allocated' -A 5 | grep 'Name\|cpu'
kubectl describe nodes | grep 'Name:\|Allocated' -A 5 | grep 'Name\|memory'
```

## Destroy infrastructure
```bash
terraform destroy --auto-approve
```