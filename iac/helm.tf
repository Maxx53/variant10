#=====================REPO_CHARTS==================================

resource "helm_release" "cert_manager" {
  chart      = "cert-manager"
  repository = "https://charts.jetstack.io"
  name       = "cert-manager"

  create_namespace = true
  namespace        = "certmanager"

  set {
    name  = "installCRDs"
    value = "true"
  }

  depends_on = [
    module.eks
  ]
}

resource "helm_release" "ingress_nginx" {
  name             = "ingress-nginx"
  namespace        = "ingress"
  create_namespace = true
  chart            = "ingress-nginx"
  version          = "4.0.16"
  repository       = "https://kubernetes.github.io/ingress-nginx"

  # set {
  #   name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-backend-protocol"
  #   value = "tcp"
  # }
  # set {
  #   name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-cross-zone-load-balancing-enabled"
  #   value = "true"
  # }
  # set {
  #   name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-type"
  #   value = "nlb"
  # }

  depends_on = [
    helm_release.cert_manager
  ]
}

resource "helm_release" "keda-operator" {
  name       = "keda-operator"
  chart      = "keda"
  repository = "https://kedacore.github.io/charts"

  create_namespace = true
  namespace        = "keda"

  depends_on = [
    helm_release.cloudwatch
  ]
}

resource "helm_release" "sonarqube" {
  name          = "sonarqube"
  namespace     = kubernetes_namespace.tools.metadata.0.name
  repository    = "https://oteemo.github.io/charts"
  chart         = "sonarqube"
  timeout       = 1800
  force_update  = "true"
  recreate_pods = "true"

  values = [
    "${file("${var.helm_path}/sonarqube/values.yaml")}"
  ]

  depends_on = [
    helm_release.cluster_issuer
  ]

}

#=====================LOCAL_CHARTS==================================

resource "helm_release" "cluster_issuer" {
  name      = "cluster-issuer"
  namespace = "certmanager"

  chart = "${var.helm_path}/cert-issuer"

  set {
    name  = "secretName"
    value = var.cert_secret
  }
  set {
    name  = "name"
    value = var.issuer_name
  }

  depends_on = [
    helm_release.ingress_nginx
  ]
}

resource "helm_release" "cloudwatch" {
  name             = "cloudwatch"
  create_namespace = true
  namespace        = "amazon-cloudwatch"
  chart            = "${var.helm_path}/cloudwatch"

  set {
    name  = "regionName"
    value = var.region
  }
  set {
    name  = "clusterName"
    value = local.cluster_name
  }

  depends_on = [
    helm_release.cluster_issuer
  ]
}

locals {
  app_chart_path = "${var.helm_path}/${var.appname}"
}

resource "helm_release" "var10_app" {

  for_each = var.app_envs

  name      = "var10-app"
  namespace = each.key

  chart = local.app_chart_path

  values = [
    "${file("${local.app_chart_path}/values.${each.key}.yaml")}"
  ]

  depends_on = [
    helm_release.keda-operator,
    kubernetes_namespace.env_ns
  ]
}
