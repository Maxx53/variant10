output "cluster_id" {
  description = "EKS cluster ID."
  value       = module.eks.cluster_id
}

output "cluster_endpoint" {
  description = "Endpoint for EKS control plane."
  value       = module.eks.cluster_endpoint
}

output "cluster_security_group_id" {
  description = "Security group ids attached to the cluster control plane."
  value       = module.eks.cluster_security_group_id
}

output "region" {
  description = "AWS region"
  value       = var.region
}

output "cluster_name" {
  description = "Kubernetes Cluster Name"
  value       = local.cluster_name
}

output "cluster_cert_data" {
  sensitive   = true
  description = "Kubernetes Cluster certificate authority data"
  value       = data.aws_eks_cluster.cluster.certificate_authority[0].data
}

output "gitlab_token" {
  sensitive   = true
  description = "GitLab k8s service account token"
  value       = lookup(data.kubernetes_secret.gitlab_sa_secret.data, "token")
}

output "admin_token" {
  sensitive   = true
  description = "Admin k8s service account token"
  value       = lookup(data.kubernetes_secret.admin_sa_secret.data, "token")
}

output "domain" {
  description = "Domain added to hosted zone, refered to load balancer"
  value       = var.domain
}

output "load-balancer-hostname" {
  value = data.kubernetes_service.ingress_nginx.status[0].load_balancer[0].ingress[0].hostname
}

output "rds_connection" {
  description = "base64 encoded connection string"
  value = [
    for conn in kubernetes_secret.rds_connection : conn.data.string
  ]

  sensitive = true
}
