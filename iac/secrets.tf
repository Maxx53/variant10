resource "kubernetes_secret" "rds_connection" {

  for_each = var.app_envs

  metadata {
    name      = "rds-connection"
    namespace = kubernetes_namespace.env_ns[each.key].metadata.0.name
    labels = {
      "sensitive" = "true"
      "app"       = "${var.appname}"
    }
  }

  data = {
    string = base64encode("server=${kubernetes_service.rds_service[each.key].metadata.0.name};database=${var.db_name};user=${var.db_user};password=${random_string.mysql_password[each.key].result}")
  }
}


data "kubernetes_secret" "gitlab_sa_secret" {
  metadata {
    name = kubernetes_service_account.gitlab_service_account.default_secret_name
  }
}

data "kubernetes_secret" "admin_sa_secret" {
  metadata {
    name = kubernetes_service_account.admin_service_account.default_secret_name
  }
}
