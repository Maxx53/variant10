resource "kubernetes_namespace" "env_ns" {

  for_each = var.app_envs

  metadata {
    annotations = {
      name = each.key
    }

    labels = {
      mylabel = each.key
    }

    name = each.key
  }
}

resource "kubernetes_namespace" "tools" {
  metadata {
    annotations = {
      name = "tools"
    }

    labels = {
      mylabel = "tools"
    }

    name = "tools"
  }
}

