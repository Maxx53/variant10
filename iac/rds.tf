resource "random_string" "mysql_password" {
  for_each = var.app_envs
  length   = 10
  special  = false
}

resource "aws_db_parameter_group" "var10_rds_params" {
  name   = "var10-rds-params"
  family = "mysql8.0"

  parameter {
    name  = "character_set_server"
    value = "utf8"
  }

  parameter {
    name  = "character_set_client"
    value = "utf8"
  }
}

resource "aws_db_instance" "var10_mysql" {
  for_each = var.app_envs

  allocated_storage      = 10
  engine                 = "mysql"
  engine_version         = "8.0"
  instance_class         = "db.t3.micro"
  port                   = "3306"
  identifier             = "mysql${each.key}"
  identifier_prefix      = null
  multi_az               = false
  storage_encrypted      = false
  skip_final_snapshot    = true
  snapshot_identifier    = null
  username               = var.db_user
  password               = random_string.mysql_password[each.key].result
  db_name                = var.db_name
  parameter_group_name   = aws_db_parameter_group.var10_rds_params.name
  db_subnet_group_name   = aws_db_subnet_group.rds-subnet.id
  vpc_security_group_ids = ["${aws_security_group.rds-sg.id}"]

  tags = {
    Name = "var10-mysql-${each.key}"
  }
}
