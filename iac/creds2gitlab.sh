#!/bin/bash

# Make sure needed bins installed: apt install curl jq -y
# Use this script to push k8s creds from terraform to gitlab CI/CD variables
# Make sure you run 'terraform init' to sync with remote backend before this script
# export GL_API_TOKEN=your-token

PROJECT_ID="31673701"
API_URL="https://gitlab.com/api/v4"
SONAR_URL="https://sonar.v10.fun"
TOKEN_NAME="var10_analyze"

if [ -z "$GL_API_TOKEN" ]
then
	echo "\$GL_API_TOKEN is not set!"
	exit 1
fi

function CurlGitlabApiEdit ()
{
	curl --request PUT --header "PRIVATE-TOKEN: $GL_API_TOKEN" "$API_URL/projects/$PROJECT_ID/variables/$1" --form "value=$2" --form "protected=false" --form "masked=true" > /dev/null 2>&1
}

function CurlGitlabApiCreate ()
{
	curl --request POST --header "PRIVATE-TOKEN: $GL_API_TOKEN" "$API_URL/projects/$PROJECT_ID/variables/" --form "key=$1" --form "value=$2" --form "protected=false" --form "masked=true" > /dev/null 2>&1
}

for var in cluster_endpoint cluster_cert_data gitlab_token; do
	echo "Setting $var..."

	# To create vars
	# CurlGitlabApiCreate ${var^^} $(terraform output $var | tr -d '"')

	# Updating existing vars
	CurlGitlabApiEdit ${var^^} $(terraform output $var | tr -d '"')
done


# You can skip token generation if you already have $sonarToken in env
echo "Generating sonar token..."
sonarToken=$(curl -X POST -u admin:admin $SONAR_URL/api/user_tokens/generate -d "name=$TOKEN_NAME" 2>/dev/null | jq -r '.token')

if [ -z "$sonarToken" ]
then
	echo "\$sonarToken - error generate or not exist!"
	exit 1
else
	echo "Setting sonar token..."
	# To create var
	# CurlGitlabApiCreate "SONAR_TOKEN" $sonarToken
	# CurlGitlabApiCreate "SONAR_HOST_URL" $SONAR_URL
	
	CurlGitlabApiEdit "SONAR_TOKEN" $sonarToken
	CurlGitlabApiEdit "SONAR_HOST_URL" $SONAR_URL
fi

echo "Done!"