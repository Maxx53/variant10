using System.Threading.Tasks;
using Variant10.DbModel;
using Xunit;

namespace Variant10.DbModelTests
{
    public class UnitTestCrud
    {
        [Fact]
        public async Task Test1Async()
        {
            //arrange

            //act

            //create and read
            var helper = new TestDbHelper<Game>();
            var gameToWrite = new Game() { Id = 1, Pk = 123, Status = "test1" };
            await helper.Add(gameToWrite);
            var gameExisting = await helper.GetById(1);

            //update
            var gameToUpdate = new Game() { Id = 3, Pk = 111, Status = "test2" };
            await helper.Add(gameToUpdate);
            var pkBefore = (await helper.GetById(3)).Pk;          
            gameToUpdate.Pk = 777;
            await helper.Update(gameToUpdate);
            var pkAfter = (await helper.GetById(3)).Pk;

            //remove
            var gameToRemove = new Game() { Id = 2, Pk = 321, Status = "test3" };
            await helper.Add(gameToRemove);
            await helper.Remove(gameToRemove);
            var gameRemoved = await helper.GetById(2);

            //assert
            Assert.NotNull(helper);
            Assert.NotNull(gameToWrite);
            Assert.NotNull(gameExisting);
            Assert.Equal(123, gameExisting.Pk);
            Assert.Equal("test1", gameExisting.Status);
            Assert.NotEqual(pkBefore, pkAfter);
            Assert.Null(gameRemoved);
        }
    }
}
