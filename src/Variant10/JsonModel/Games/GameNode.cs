﻿using System.Text.Json.Serialization;

namespace Variant10.JsonModel.Games
{
    class GameNode
    {
        [JsonPropertyName("gamePk")]
        public int GamePk { get; set; }
        [JsonPropertyName("gameDate")]
        public string GameDate { get; set; }
        [JsonPropertyName("venue")]
        public VenueInfo Venue { get; set; }
        [JsonPropertyName("status")]
        public StatusInfo Status { get; set; }
    }
}
