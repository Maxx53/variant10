﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Variant10.JsonModel.Games
{
    class GamesRoot
    {
        [JsonPropertyName("dates")]
        public List<DateNode> Dates { get; set; }
    }
}
