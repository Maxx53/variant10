﻿using System.Text.Json.Serialization;

namespace Variant10.JsonModel.Games
{
    public class VenueInfo
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; }
    }
}