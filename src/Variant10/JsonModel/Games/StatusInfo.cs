﻿using System.Text.Json.Serialization;

namespace Variant10.JsonModel.Games
{
    public class StatusInfo
    {
        [JsonPropertyName("detailedState")]
        public string DetailedState { get; set; }
    }
}