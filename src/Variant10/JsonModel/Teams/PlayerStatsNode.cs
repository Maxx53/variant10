﻿using System.Text.Json.Serialization;

namespace Variant10.JsonModel.Teams
{
    public class PlayerStatsNode
    {
        [JsonPropertyName("goalieStats")]
        public StatsInfo GoalieStats { get; set; }
        [JsonPropertyName("skaterStats")]
        public StatsInfo SkaterStats { get; set; }
    }
}