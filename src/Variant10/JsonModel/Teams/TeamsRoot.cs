﻿using System.Text.Json.Serialization;

namespace Variant10.JsonModel.Teams
{
    class TeamsRoot
    {
        [JsonPropertyName("teams")]
        public TeamsNode Teams { get; set; }
    }
}
