﻿using System.Text.Json.Serialization;

namespace Variant10.JsonModel.Teams
{
    public class TeamStatsNode
    {
        [JsonPropertyName("teamSkaterStats")]
        public TeamStatsInfo TeamSkaterStats { get; set; }
    }
}