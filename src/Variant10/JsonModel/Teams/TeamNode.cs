﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Variant10.JsonModel.Teams
{
    public class TeamNode
    {
        [JsonPropertyName("team")]
        public TeamInfo Team { get; set; }
        [JsonPropertyName("teamStats")]
        public TeamStatsNode TeamStats { get; set; }
        [JsonPropertyName("players")]
        public Dictionary<string, PlayeNode> Players { get; set; }
    }
}