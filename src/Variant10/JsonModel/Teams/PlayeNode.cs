﻿using Variant10.DbModel;
using System.Text.Json.Serialization;

namespace Variant10.JsonModel.Teams
{
    public class PlayeNode
    {
        [JsonPropertyName("person")]
        public PersonInfo Person { get; set; }
        [JsonPropertyName("position")]
        public PositionInfo Position { get; set; }
        [JsonPropertyName("stats")]
        public PlayerStatsNode Stats { get; set; }


        public static explicit operator Player(PlayeNode obj)
        {
            return new Player()
            {
                Name = obj.Person.FullName,
                Number = obj.Person.PrimaryNumber,
                Position = obj.Person.PrimaryPosition.Name,
                TeamId = obj.Person.CurrentTeam.Id,
                TimeOnIce = obj.Stats.GoalieStats?.TimeOnIce ?? obj.Stats.SkaterStats?.TimeOnIce
            };

        }
    }
}