﻿using System.Text.Json.Serialization;

namespace Variant10.JsonModel.Teams
{
    public class StatsInfo
    {
        [JsonPropertyName("timeOnIce")]
        public string TimeOnIce { get; set; }
    }
}