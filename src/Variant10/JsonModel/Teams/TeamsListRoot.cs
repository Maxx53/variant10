﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Variant10.JsonModel.Teams
{
    class TeamsListRoot
    {
        [JsonPropertyName("teams")]
        public List<TeamInfo> Teams { get; set; }
    }
}
