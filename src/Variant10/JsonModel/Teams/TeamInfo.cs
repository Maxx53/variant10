﻿using System.Text.Json.Serialization;
using Variant10.DbModel;

namespace Variant10.JsonModel.Teams
{
    public class TeamInfo
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; }

        public static explicit operator Team(TeamInfo obj)
        {
            return new Team() { Id = obj.Id, Name = obj.Name };
        }
    }
}