﻿using System.Text.Json.Serialization;

namespace Variant10.JsonModel.Teams
{
    public class TeamStatsInfo
    {
        [JsonPropertyName("goals")]
        public int Goals { get; set; }
        [JsonPropertyName("shots")]
        public int Shots { get; set; }
        [JsonPropertyName("hits")]
        public int Hits { get; set; }
    }
}