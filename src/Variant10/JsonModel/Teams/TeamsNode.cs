﻿using System.Text.Json.Serialization;

namespace Variant10.JsonModel.Teams
{
    class TeamsNode
    {
        [JsonPropertyName("away")]
        public TeamNode Away { get; set; }
        [JsonPropertyName("home")]
        public TeamNode Home { get; set; }
    }
}
