﻿using System.Text.Json.Serialization;

namespace Variant10.JsonModel.Teams
{
    public class PositionInfo
    {
        [JsonPropertyName("code")]
        public string Code { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; }
    }
}