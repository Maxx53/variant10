﻿namespace Variant10.NhlApi
{
    public class NhlApiSettings
    {
        public string ApiRoot { get; set; }
        public int VenueId { get; set; }
        public string VenueName { get; set; }
        public int HomeId { get; set; }
    }
}
