﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Variant10.DbModel;
using Variant10.JsonModel.Games;
using Variant10.JsonModel.Teams;

namespace Variant10.NhlApi
{
    public class NhlApiService : INhlApiService
    {
        private readonly ILogger<NhlApiService> _logger;
        private readonly IHttpClientFactory _httpFactory;

        private readonly string _apiRoot;
        private readonly int _venueId;
        private readonly string _venueName;
        private readonly int _homeId;

        private readonly SemaphoreSlim _slimSem;

        public NhlApiService(ILogger<NhlApiService> logger, IOptions<NhlApiSettings> options, IHttpClientFactory httpFactory)
        {
            _logger = logger;
            _httpFactory = httpFactory;
            _apiRoot = options.Value.ApiRoot;
            _venueId = options.Value.VenueId;
            _venueName = options.Value.VenueName;
            _homeId = options.Value.HomeId;
            _slimSem = new SemaphoreSlim(8);
        }

        public async Task<List<Game>> GetGamesAsync(int month)
        {
            List<Game> games = new();
            List<Task> tasks = new();

            _logger.LogInformation($"Getting data for: {_venueName}, id {_venueId}");

            var AllGames = await GetAsync<GamesRoot>(GenGamesUrl(month, _apiRoot, _homeId));
            var FilteredGames = AllGames.Dates.SelectMany(d => d.Games).Where(g => g.Venue.Id == _venueId);

            foreach (var game in FilteredGames)
            {
                tasks.Add(Task.Run(async () =>
                {
                    try
                    {
                        await _slimSem.WaitAsync();

                        var teams = await GetAsync<TeamsRoot>($"{_apiRoot}/game/{game.GamePk}/boxscore");

                        var plist = teams.Teams.Away.Players.Values.Concat(teams.Teams.Home.Players.Values).Where(p => p.Stats.GoalieStats != null || p.Stats.SkaterStats != null);
                        var topFiltered = plist.OrderBy(p => p.Stats.GoalieStats?.TimeOnIce ?? p.Stats.SkaterStats?.TimeOnIce, new TimeComparer()).Take(3).ToList();

                        var homeTeam = (Team)teams.Teams.Home.Team;
                        var awayTeam = (Team)teams.Teams.Away.Team;

                        var topPlayers = new List<Player>();

                        for (int i = 0; i < topFiltered.Count; i++)
                        {
                            var topPlayer = (Player)topFiltered[i];
                            topPlayer.Top = i + 1;
                            topPlayers.Add(topPlayer);
                        }

                        var newGame = new Game()
                        {
                            AwayId = awayTeam.Id,
                            HomeId = homeTeam.Id,
                            Pk = game.GamePk,
                            AwayScore = teams.Teams.Away.TeamStats.TeamSkaterStats.Goals,
                            HomeScore = teams.Teams.Home.TeamStats.TeamSkaterStats.Goals,
                            GameDate = DateTime.Parse(game.GameDate),
                            TimeStamp = DateTime.Now,
                            TopPlayers = topPlayers,
                            Status = game.Status.DetailedState
                        };

                        games.Add(newGame);
                        _logger.LogInformation($"Game added: {game.GamePk}");

                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"Error processing game {game.GamePk}: {ex.Message}");
                    }
                    finally
                    {
                        _slimSem.Release();
                    }
                }));
            }

            Task.WaitAll(tasks.ToArray());

            return games.OrderBy(x => x.GameDate).ToList();
        }

        public static string GenGamesUrl(int month, string apiRoot, int id)
        {
            var today = DateTime.Today;
            var currMonth = new DateTime(today.Year, today.Month, 1);
            var reqMonth = currMonth.AddMonths(-month);
            var first = reqMonth.ToString("yyyy-MM-dd");
            var last = reqMonth.AddMonths(1).AddDays(-1).ToString("yyyy-MM-dd");

            return $"{apiRoot}/schedule?startDate={first}&endDate={last}&teamId={id}";
        }

        public async Task<T> GetAsync<T>(string uri)
        {
            using var httpClient = _httpFactory.CreateClient("Var10Client");
            var response = await httpClient.GetAsync(uri);
            var content = await response.Content.ReadAsStreamAsync();

            return await JsonSerializer.DeserializeAsync<T>(content);
        }

        public async Task<List<Team>> GetTeamsAsync()
        {
            List<Team> teams = new();

            _logger.LogInformation($"Getting team info...");

            var AllTeams = await GetAsync<TeamsListRoot>($"{_apiRoot}/teams");

            foreach (var team in AllTeams.Teams)
            {
                teams.Add((Team)team);
            }

            return teams;
        }
    }
}
