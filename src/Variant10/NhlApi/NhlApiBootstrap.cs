﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Variant10.NhlApi
{
    public static class NhlApiBootstrap
    {
        public static void UseNhlApi(this IServiceCollection services, IConfiguration config)
        {
            services.Configure<NhlApiSettings>(config);
            services.AddScoped<INhlApiService, NhlApiService>();
        }
    }
}
