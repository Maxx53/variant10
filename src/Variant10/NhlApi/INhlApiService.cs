﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Variant10.DbModel;

namespace Variant10.NhlApi
{
    public interface INhlApiService
    {
        Task<List<Game>> GetGamesAsync(int month);
        Task<List<Team>> GetTeamsAsync();
    }
}
