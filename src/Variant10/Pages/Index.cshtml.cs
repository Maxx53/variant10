﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Variant10.DbModel;
using Variant10.NhlApi;

namespace Variant10.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly GamesContext _gamesContext;
        private readonly INhlApiService _nhlApi;

        [BindProperty]
        public IEnumerable<Game> Games { get; set; }

        public const string MessageKey = nameof(MessageKey);
        public const bool DbError = false;

        public IndexModel(ILogger<IndexModel> logger, GamesContext context, INhlApiService nhlapi)
        {
            _logger = logger;
            _gamesContext = context;
            _nhlApi = nhlapi;
        }

        private void SetDBerror()
        {
            TempData[nameof(DbError)] = true;
            TempData[MessageKey] = _gamesContext.DataSource;
        }

        public async Task OnGetAsync(int month = 1, bool table = true)
        {
            if (_gamesContext.IsAvailable)
            {
                try
                {
                    if (_gamesContext.Games.Any())
                    {
                        _logger.LogInformation("Getting games...");

                        //Need to explicit load teams as top players can be from any team (not only from home or away)!
                        _gamesContext.Teams.Load();

                        var games = await _gamesContext.Games.Include(t => t.TopPlayers).ToListAsync();

                        //Fix month 01 - 1 = 12
                        var reqDate = DateTime.Now.AddMonths(-month);
                        Games = games.Where(d => d.GameDate.Month == reqDate.Month && d.GameDate.Year == reqDate.Year);

                        if (!Games.Any())
                            TempData[MessageKey] = $"No games matching {Utils.GetPrettyDate(reqDate)}, update from API!";
                    }
                    else
                        TempData[MessageKey] = "No games in DB, update from API!";

                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    SetDBerror();
                }
            }
            else
            {
                SetDBerror();
            }

            TempData[nameof(table)] = table;
            TempData[nameof(month)] = month;
        }

        public async Task<IActionResult> OnPostUpdate(int month = 1)
        {
            if (_gamesContext.IsAvailable)
            {
                await EnsureTeams();

                _logger.LogInformation("Updating data...");
                var games = await _nhlApi.GetGamesAsync(month);

                foreach (var game in games)
                {
                    var existingGame = _gamesContext.Games.SingleOrDefault(g => g.Pk == game.Pk);

                    if (existingGame != null)
                    {
                        _logger.LogInformation($"Game alredy exist in db: {game.Pk}");
                        continue;
                    }

                    _gamesContext.Games.Add(game);
                }

                await _gamesContext.SaveChangesAsync();

                TempData[MessageKey] = "Database updated from API";
            }

            return RedirectToAction(Request.Path, new { table = Request.Query["table"].ToString(), month = Request.Query["month"].ToString() });
        }

        private async Task EnsureTeams()
        {
            if (!_gamesContext.Teams.Any())
            {
                var teams = await _nhlApi.GetTeamsAsync();

                foreach (var team in teams)
                {
                    _gamesContext.Teams.Add(team);
                }

                await _gamesContext.SaveChangesAsync();
            }
        }

        public async Task<IActionResult> OnPostClear()
        {
            if (_gamesContext.IsAvailable)
            {
                _logger.LogInformation("Clearing data...");
                _gamesContext.Games.RemoveRange(_gamesContext.Games);
                await _gamesContext.SaveChangesAsync();
            }

            return RedirectToAction(Request.Path, new { table = Request.Query["table"].ToString() });
        }

        public async Task<IActionResult> OnPostReconnect()
        {
            _logger.LogInformation("Reconnecting to DB...");

            _gamesContext.IsAvailable = await _gamesContext.Database.CanConnectAsync();

            if (_gamesContext.IsAvailable)
                _gamesContext.Database.EnsureCreated();

            return RedirectToAction(Request.Path);
        }
    }
}
