﻿using System;
using System.Globalization;

namespace Variant10
{
    public static class Utils
    {
        public static string GetPrettyDate(DateTime date)
        {
            return date.ToString("MMMM yyyy", CultureInfo.InvariantCulture);
        }
    }
}
