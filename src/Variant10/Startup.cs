using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Data.Common;
using System.Text;
using Variant10.DbModel;
using Variant10.NhlApi;

namespace Variant10
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connString = GetConnectionStr();

            if (string.IsNullOrEmpty(connString))
            {
                Console.WriteLine("NULL or invalid connection string!");
            }
            else
            {
                services.AddDbContext<GamesContext>(options =>
                 options.UseMySQL(connString));
            }

            services.AddHealthChecks().AddDbContextCheck<GamesContext>();
            services.AddHttpClient();
            services.UseNhlApi(Configuration.GetSection("NhlApiSettings"));
            services.AddRazorPages().AddRazorPagesOptions(options =>
            {
                options.Conventions
                       .ConfigureFilter(new IgnoreAntiforgeryTokenAttribute());
            });
        }

        private string GetConnectionStr()
        {
            var connStr = Configuration["VAR10_CONN"];

            try
            {
                connStr = Encoding.UTF8.GetString(Convert.FromBase64String(connStr));
                DbConnectionStringBuilder csb = new()
                {
                    ConnectionString = connStr
                };

                if (csb.Count == 0)
                    Console.WriteLine("Connection string is empty!");
            }
            catch
            {
                connStr = null;
            }

            return connStr;
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapHealthChecks("/health");
            });
        }
    }
}
