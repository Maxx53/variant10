﻿using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;

namespace Variant10.DbModel
{
    public class GamesContext : DbContext
    {
        public DbSet<Game> Games { get; set; }
        public DbSet<Team> Teams { get; set; }
        public bool IsAvailable { get; set; }
        public string DataSource { get; set; }

        public GamesContext(DbContextOptions<GamesContext> options) : base(options)
        {
            if (Database.IsRelational())
            {
                DataSource = (new SqlConnectionStringBuilder(Database.GetConnectionString())).DataSource;
                IsAvailable = Database.CanConnect();

                if (IsAvailable)
                    Database.EnsureCreated();
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            _ = modelBuilder.Entity<Team>(entity =>
              {
                  _ = entity.HasKey(e => e.Id);
                  _ = entity.Property(e => e.Name).IsRequired();
                  _ = entity.HasIndex(e => e.Name).IsUnique();
              });

            _ = modelBuilder.Entity<Player>(entity =>
              {
                  _ = entity.HasKey(e => e.Id);
                  _ = entity.Property(e => e.Name).IsRequired();
                  _ = entity.Property(e => e.TimeOnIce).IsRequired();
                  _ = entity.HasOne(d => d.Game)
                    .WithMany(p => p.TopPlayers);
              });


            _ = modelBuilder.Entity<Game>(entity =>
              {
                  _ = entity.HasKey(e => e.Id);
                  _ = entity.Property(e => e.HomeScore).IsRequired();
                  _ = entity.Property(e => e.AwayScore).IsRequired();
                  _ = entity.Property(e => e.GameDate).IsRequired();

              });

        }
    }
}
