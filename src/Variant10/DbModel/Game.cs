﻿using System;
using System.Collections.Generic;

namespace Variant10.DbModel
{
    public class Game
    {
        public int Id { get; set; }
        public int Pk { get; set; }
        public int HomeId { get; set; }
        public int AwayId { get; set; }
        public int HomeScore { get; set; }
        public int AwayScore { get; set; }
        public string Status { get; set; }
        public DateTime GameDate { get; set; }
        public DateTime TimeStamp { get; set; }
        public virtual Team Home { get; set; }
        public virtual Team Away { get; set; }
        public virtual List<Player> TopPlayers { get; set; }
    }
}
