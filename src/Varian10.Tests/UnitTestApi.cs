using System.Threading.Tasks;
using Xunit;
namespace Variant10.Tests
{
    public class UnitTestApi
    {
        [Fact]
        public async Task Test1Async()
        {
            //arrange
   
            var helper = new TestHelper("appsettings.json");
            helper.SetupClient("{\"teams\":[{\"id\":19,\"name\":\"St. Louis Blues\"}]}");

            //act

            var api = helper.GetApi();
            var teams = await api.GetTeamsAsync();

            //assert

            Assert.NotNull(api);
            Assert.Equal(5076, helper.nhlSettings.Value.VenueId);
            Assert.Equal("Enterprise Center", helper.nhlSettings.Value.VenueName);
            Assert.Single(teams);
            Assert.Equal(19, teams[0].Id);
            Assert.Equal("St. Louis Blues", teams[0].Name);
        }
    }
}
