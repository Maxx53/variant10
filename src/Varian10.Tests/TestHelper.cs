﻿
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Moq.Protected;
using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Variant10.NhlApi;

namespace Variant10.Tests
{
    class TestHelper
    {
        public readonly IOptions<NhlApiSettings> nhlSettings;
        public Mock<IHttpClientFactory> MockFactory { get; private set; }

        public TestHelper(string settings)
        {
            //reading JSON config
            var config = new ConfigurationBuilder()
              .SetBasePath(AppContext.BaseDirectory)
              .AddJsonFile(settings, optional: true, reloadOnChange: true)
              .Build();

            nhlSettings = Options.Create(new NhlApiSettings());
            config.GetSection("NhlApiSettings").Bind(nhlSettings.Value);
        }

        internal void SetupClient(string expected)
        {
            MockFactory = new Mock<IHttpClientFactory>();

            var mockMessageHandler = new Mock<HttpMessageHandler>();
            mockMessageHandler.Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(expected)
                });

            var httpClient = new HttpClient(mockMessageHandler.Object);
            MockFactory.Setup(_ => _.CreateClient(It.IsAny<string>())).Returns(httpClient);
        }


        public NhlApiService GetApi()
        {
            var logger = new Mock<ILogger<NhlApiService>>();
            return new NhlApiService(logger.Object, nhlSettings, MockFactory.Object);
        }
    }
}
