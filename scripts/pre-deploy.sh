#!/bin/bash

envtype=$1

# Put resources and their names here
# Namespace is operating in own way

declare -A checks=(
[secret]="rds-connection"
[service]="rds-service"
[ns]=""
)

. check-resources.sh