#!/bin/bash

# Use this script to set kubectl config using gitlab variables

if [ -z "$1" ]
  then
    echo "No config name supplied!"
	exit 1
fi


kubectl config set-cluster k8s --server="${CLUSTER_ENDPOINT}"
kubectl config set clusters.k8s.certificate-authority-data ${CLUSTER_CERT_DATA}
kubectl config set-credentials $1 --token="${GITLAB_TOKEN}"
kubectl config set-context default --cluster=k8s --user=$1
kubectl config use-context default

if [ "${CI_COMMIT_BRANCH}" == "main" ]; then
	export envtype=prod
	export tagName=$CI_COMMIT_SHORT_SHA
elif [ ! -z "$CI_COMMIT_TAG" ]; then
    export envtype=prod
	export tagName=$CI_COMMIT_TAG
else
	export envtype=dev
	export tagName=$CI_COMMIT_SHORT_SHA
fi

echo "enviroment: $envtype"
echo "image tag: $tagName"