#!/bin/bash
var10path="/publish"
var10dll="$var10path/Variant10.dll"
echo "Checking files..."
ls -lah $var10path
[ -f $var10dll ] && echo "$var10dll exist." || (echo "$var10dll does not exist!"; exit 1)
echo "App run check!"
dotnet $var10dll &
apt -qq update && apt -qq install lsof netcat -y
echo "Checking port 5000..."
lsof -i tcp:5000
nc -zv 127.0.0.1 5000