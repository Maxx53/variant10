#!/bin/bash

status=0
envtype=$1
site="https://$envtype.v10.fun"

# Put resources and their names here

declare -A checks=(
[deploy]="var10-app"
[service]="var10-service"
[ingress]="var10-ingress"
[scaledobject]="keda-scaler"
[hpa]="keda-hpa-keda-scaler"
)

. check-resources.sh


obj=${checks[scaledobject]}
metric=$(kubectl get scaledobject $obj -n $envtype -o jsonpath={.status.externalMetricNames[0]})

echo "Checking external metric $metric..."
metricStatus=$(kubectl get scaledobject -n $envtype -o jsonpath={.items[0].status.health.${metric,,}.status})

if [[ $metricStatus == "Happy" ]]
then
  echo "Check OK!"
else
  echo "Check FAILED! Status: $metricStatus"
  status=1
fi

echo "Site health check..."
siteHealth=$(curl -s $site/health)
if [[ $siteHealth == "Healthy" ]]
then
  echo "Check OK!"
else
  echo "Check FAILED! Status: $siteHealth"
  status=1
fi

echo "Check main page status..."
mainCode=$(curl -o /dev/null -s -w "%{http_code}\n" $site)
if [[ $mainCode == "200" ]]
then
  echo "Check OK!"
else
  echo "Check FAILED! Code: $mainCode"
  status=1
fi

exit $status