#!/bin/bash

#This script made for checking resource availability in k8s cluster
#Pass checks array and envtype string using source command

for key in "${!checks[@]}"; do

        object=${checks[$key]}

        echo "Checking $key in -n=$envtype"

        # Namespace operating in own way
        if [[ $key == "ns" ]]
        then
          ns=""
          comp=$envtype
        else
          ns="-n $envtype"
          comp=$object
        fi

        result=$(kubectl get $key $comp $ns -o=jsonpath='{.metadata.name}')

        if [[ "$result" == "$comp" ]]
        then
                echo "$comp: OK!"
        else
                echo "$comp: FAIL! Result: $result"
                exit 1
        fi
done