## Build
```bash
docker build -t registry.gitlab.com/maxx53/variant10/tflint-bundle:latest .
```

## Push
```bash
docker login registry.gitlab.com -u maxx53
docker push registry.gitlab.com/maxx53/variant10/tflint-bundle:latest
```

## Check
```bash
docker run -v /absolute_path/variant10/iac:/data -it registry.gitlab.com/maxx53/variant10/tflint-bundle
```