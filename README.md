## Introduction

Made with .NET 5.0.12. Used libraries and tools:

* Entity Framework
* MySQL
* Bootstrap

# Resource Links

- [v10.fun](https://v10.fun/)
- [prod.v10.fun](https://prod.v10.fun/)
- [dev.v10.fun](https://dev.v10.fun/)
- [sonar.v10.fun](https://sonar.v10.fun/)

# Autoscaling test

### Prod KEDA scaling with AWS/ELB RequesCount metric

```bash
for i in {1..300}; do curl -s https://v10.fun/ -o /dev/null; done &

kubectl get scaledobject -n prod
kubectl get --raw "/apis/external.metrics.k8s.io/v1beta1/namespaces/prod/s0-aws-cloudwatch-Namespace" | jq
kubectl get hpa -n prod
kubectl describe hpa -n prod
kubectl get pods -n prod
```

### Dev KEDA scaling with ContainerInsights pod_cpu_utilization_over_pod_limit metric

```bash
kubectl get pods -n dev
kubectl exec -n dev -it var10-app-... -- bash
apt update; apt install stress htop -y; stress -c 2 &
htop
kill -9 $(pidof stress)

kubectl get scaledobject -n dev
kubectl get --raw "/apis/external.metrics.k8s.io/v1beta1/namespaces/dev/s0-aws-cloudwatch-ClusterName" | jq
kubectl get hpa -n dev
kubectl describe hpa -n dev
kubeclt get pods -n dev
```